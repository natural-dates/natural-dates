package org.naturaldates.model;

import java.util.Objects;

public class Second extends Minute {
    public final int second;

    public Second(int year, int month, int day, int hour, int minute, int second) {
        super(year, month, day, hour, minute);
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Second)) return false;
        if (!super.equals(o)) return false;
        Second second1 = (Second) o;
        return second == second1.second;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), second);
    }
}
