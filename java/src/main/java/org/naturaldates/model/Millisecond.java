package org.naturaldates.model;


import java.util.Objects;

public class Millisecond extends Second {
    public final int millisecond;

    public Millisecond(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        super(year, month, day, hour, minute, second);
        this.millisecond = millisecond;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Millisecond)) return false;
        if (!super.equals(o)) return false;
        Millisecond that = (Millisecond) o;
        return millisecond == that.millisecond;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), millisecond);
    }
}
