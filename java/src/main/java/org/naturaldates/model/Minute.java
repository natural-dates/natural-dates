package org.naturaldates.model;


import java.util.Objects;

public class Minute extends Hour {
    public final int minute;

    public Minute(int year, int month, int day, int hour, int minute) {
        super(year, month, day, hour);
        this.minute = minute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Minute)) return false;
        if (!super.equals(o)) return false;
        Minute minute1 = (Minute) o;
        return minute == minute1.minute;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), minute);
    }
}
