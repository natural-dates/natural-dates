package org.naturaldates.model;

import java.util.Objects;

public class Month extends Year {
    public final int month;

    public Month(int year, int month) {
        super(year);
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Month)) return false;
        if (!super.equals(o)) return false;
        Month month1 = (Month) o;
        return month == month1.month;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), month);
    }
}
