package org.naturaldates.model;

import java.util.Objects;

public class Day extends Month {
    public final int day;

    public Day(int year, int month, int day) {
        super(year, month);
        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Day)) return false;
        if (!super.equals(o)) return false;
        Day day1 = (Day) o;
        return day == day1.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), day);
    }
}
