package org.naturaldates.model;


import java.util.Objects;

public class Hour extends Day {
    public final int hour;

    public Hour(int year, int month, int day, int hour) {
        super(year, month, day);
        this.hour = hour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hour)) return false;
        if (!super.equals(o)) return false;
        Hour hour1 = (Hour) o;
        return hour == hour1.hour;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hour);
    }
}
