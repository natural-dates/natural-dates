package org.naturaldates.model

import spock.lang.Specification

class DaySpecification extends Specification {

    def "should create Day object"(int y, int m, int d) {
        given:
        Day day = new Day(y, m, d)

        expect:
        day.year == y
        day.day == m
        day.day == d

        where:
        y    | m  | d
        2001 | 1  | 1
        -1   | -1 | -1
        0    | 0  | 0
    }

    def "hash codes should be equal for the same day"(int y, int m, int d) {
        given:
        Day day1 = new Day(y, m, d)
        Day day2 = new Day(y, m, d)

        expect:
        day1.hashCode() == day2.hashCode()

        where:
        y    | m | d
        2000 | 1 | -1
        9999 | 1 | 99
    }

    def "hash codes shouldn't be equal for different months"(Day day1, Day day2) {

        expect:
        day1.hashCode() != day2.hashCode()

        where:
        day1                | day2
        new Day(2000, 1, 1) | new Day(2000, 1, 2)
        new Day(2000, 1, 1) | new Day(2000, 2, 1)
        new Day(2000, 1, 1) | new Day(2001, 1, 1)

    }

    def "equals test"(Day day1, Day day2, boolean equals) {
        expect:
        day1.equals(day2) == equals

        where:
        day1                | day2                                    | equals
        new Day(2001, 1, 1) | new Day(2001, 1, 1)                     | true
        new Day(2001, 1, 1) | new Day(2001, 1, 2)                     | false
        new Day(2001, 1, 1) | new Day(2001, 2, 1)                     | false
        new Day(2001, 1, 1) | new Day(2002, 1, 1)                     | false
        new Day(2001, 1, 1) | new Hour(2001, 1, 1, 1)                 | true
        new Day(2001, 1, 1) | new Hour(2001, 1, 2, 1)                 | false
        new Day(2001, 1, 1) | new Minute(2001, 1, 1, 1, 1)            | true
        new Day(2001, 1, 1) | new Second(2001, 1, 1, 1, 1, 1)         | true
        new Day(2001, 1, 1) | new Millisecond(2001, 1, 1, 1, 1, 1, 1) | true
        new Day(2001, 1, 1) | new Millisecond(2001, 1, 2, 1, 1, 1, 1) | false
    }
}
