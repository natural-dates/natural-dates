package org.naturaldates.model

import spock.lang.Specification

class YearSpecification extends Specification {

    def "should create Year object"(int inputYear) {
        expect:
        new Year(inputYear).year == inputYear

        where:
        inputYear | _
        2001      | _
        -1        | _
        0         | _
    }

    def "hash codes should be equal for the same years"(int year) {
        expect:
        new Year(year).hashCode() == new Year(year).hashCode()

        where:
        year | _
        2000 | _
        9999 | _
    }

    def "hash codes shouldn't be equal for different years"(int year1, int year2) {
        expect:
        new Year(year1).hashCode() != new Year(year2).hashCode()

        where:
        year1 | year2
        2000  | 2001
        1     | 9999
    }

    def "equals test"(Year year1, Year year2, boolean equals) {
        expect:
        year1.equals(year2) == equals

        where:
        year1          | year2                                   | equals
        new Year(2001) | new Year(2001)                          | true
        new Year(2001) | new Year(2002)                          | false
        new Year(2001) | new Month(2001, 1)                      | true
        new Year(2001) | new Month(2002, 1)                      | false
        new Year(2001) | new Day(2001, 1, 1)                     | true
        new Year(2001) | new Hour(2001, 1, 1, 1)                 | true
        new Year(2001) | new Minute(2001, 1, 1, 1, 1)            | true
        new Year(2001) | new Second(2001, 1, 1, 1, 1, 1)         | true
        new Year(2001) | new Millisecond(2001, 1, 1, 1, 1, 1, 1) | true
        new Year(2001) | new Millisecond(2002, 1, 1, 1, 1, 1, 1) | false
    }
}
