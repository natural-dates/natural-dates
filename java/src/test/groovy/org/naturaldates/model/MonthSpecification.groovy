package org.naturaldates.model

import spock.lang.Specification

class MonthSpecification extends Specification {

    def "should create Month object"(int y, int m) {
        given:
        Month month = new Month(y, m)

        expect:
        month.year == y
        month.month == m

        where:
        y    | m
        2001 | 1
        -1   | -1
        0    | 0
    }

    def "hash codes should be equal for the same month"(int y, int m) {
        given:
        Month month1 = new Month(y, m)
        Month month2 = new Month(y, m)

        expect:
        month1.hashCode() == month2.hashCode()

        where:
        y    | m
        2000 | 1
        9999 | 1
    }

    def "hash codes shouldn't be equal for different months"(Month month1, Month month2) {

        expect:
        month1.hashCode() != month2.hashCode()

        where:
        month1             | month2
        new Month(2000, 1) | new Month(2000, 2)
        new Month(2001, 1) | new Month(2002, 1)
    }

    def "equals test"(Month month1, Month month2, boolean equals) {
        expect:
        month1.equals(month2) == equals

        where:
        month1             | month2                                  | equals
        new Month(2001, 1) | new Month(2001, 1)                      | true
        new Month(2001, 1) | new Month(2001, 2)                      | false
        new Month(2001, 1) | new Month(2002, 1)                      | false
        new Month(2001, 1) | new Day(2001, 1, 1)                     | true
        new Month(2001, 1) | new Day(2001, 2, 1)                     | false
        new Month(2001, 1) | new Hour(2001, 1, 1, 1)                 | true
        new Month(2001, 1) | new Minute(2001, 1, 1, 1, 1)            | true
        new Month(2001, 1) | new Second(2001, 1, 1, 1, 1, 1)         | true
        new Month(2001, 1) | new Millisecond(2001, 1, 1, 1, 1, 1, 1) | true
        new Month(2001, 1) | new Millisecond(2001, 2, 1, 1, 1, 1, 1) | false
    }
}
